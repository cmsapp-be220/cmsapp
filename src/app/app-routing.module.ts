import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { APP_ROUTES } from './constants/storage/app-routes.const';

const routes: Routes = [
  {
      path: APP_ROUTES.ACCOUNT,
      loadChildren: () =>
          import('./modules/account/account.module').then(
              (m) => m.AccountModule
          ),
  },
  {
      path: APP_ROUTES.MAIN,
      loadChildren: () =>
          import('./modules/main/main.module').then((m) => m.MainModule),
      // canActivate: [AuthAppService],
  },
  {
      path: '',
      redirectTo: APP_ROUTES.ACCOUNT,
      pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
