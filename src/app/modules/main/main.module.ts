import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { APP_ROUTES } from 'src/app/constants/storage/app-routes.const';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        children: [
            { path: APP_ROUTES.HOME, component: HomeComponent },
            { path: APP_ROUTES.PROFILE, component: ProfileComponent },
            { path: '', redirectTo: APP_ROUTES.HOME, pathMatch: 'full' },
        ],
    },
];

@NgModule({
    declarations: [
        MainComponent,
        HomeComponent,
        // MenuComponent,
        // HeaderComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        ReactiveFormsModule,
        FormsModule,
        // BrMaskerModule,
    ],
    // providers: [{ provide: MAT_DATE_LOCALE, useValue: 'pt-BR' }],
})
export class MainModule {}