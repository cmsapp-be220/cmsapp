import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { APP_ROUTES } from "src/app/constants/storage/app-routes.const";
import { AccountComponent } from "./account.component";
import { LoginComponent } from "./login/login.component";
import { RecoverPasswordComponent } from "./recover-password/recover-password.component";

const routes: Routes = [
    {
        path: '',
        component: AccountComponent,
        children: [
            { path: APP_ROUTES.LOGIN, component: LoginComponent },
            { path: APP_ROUTES.RECOVER_PASSWORD, component: RecoverPasswordComponent },
            { path: '', redirectTo: APP_ROUTES.LOGIN, pathMatch: 'full' },
        ],
    },
];

@NgModule({
    declarations: [AccountComponent, LoginComponent, RecoverPasswordComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        ReactiveFormsModule,
        // BrMaskerModule,
        // MatInputModule,
    ],
})
export class AccountModule {}