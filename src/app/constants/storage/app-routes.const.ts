export const APP_ROUTES = {
    ACCOUNT: 'account',
    LOGIN: 'login',
    RECOVER_PASSWORD: 'recover-password',
    MAIN: 'main',
    HOME: 'home',
    PROFILE: 'profile',
};
